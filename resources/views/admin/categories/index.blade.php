@extends('admin.app') 
@section('content')
</div>
<div class="col-sm-12">
  @if (session()->has('message'))
  <div class="alert alert-success">
    {{session('message')}}
  </div>
  @endif
</div>
<div class="row">
  <div class="col-sm-12">
    <a href="{{route('admin.category.create')}}" class="btn btn-primary float-right">Add Category</a>
  </div>
</div>
<div class="table-responsive pt-2">
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th>S.N</th>
        <th>Title</th>
        <th>Description</th>
        <th>Slug</th>
        <th>Categories</th>
        <th>Created At</th>
        <th class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
      @if ($categories) @foreach ($categories as $category)
      <tr>
        <td>{{$category->id}}</td>
        <td>{{$category->title}}</td>
        <td>{!! htmlspecialchars_decode($category->description) !!}</td>
        <td>{{$category->slug}}</td>
        <td>
          @if ($category->childrens()->count() > 0) @foreach ($category->childrens as $children) {{$children->title}}, @endforeach
          @else
          <strong>{{"Parent Category"}}</strong> @endif
        </td>
        @if($category->trashed())
        <td>{{$category->created_at}}</td>
        <td><a class="btn btn-info btn-sm" href="{{route('admin.category.recover',$category->id)}}">Restore</a> | <a class="btn btn-danger btn-sm"
            href="javascript:;" onclick="confirmDelete('{{$category->id}}')">Delete</a>
          <form id="delete-category-{{$category->id}}" action="{{ route('admin.category.destroy', $category->id) }}" method="POST"
            style="display: none;">
            @method('DELETE') @csrf
          </form>
        </td>
        @else
        <td>{{$category->created_at}}</td>
        <td><a class="btn btn-info btn-sm" href="{{route('admin.category.edit',$category->slug)}}">Edit</a> | <a id="trash-category-{{$category->id}}"
            class="btn btn-warning btn-sm" href="{{route('admin.category.remove',$category->slug)}}">Trash</a> | <a class="btn btn-danger btn-sm"
            href="javascript:;" onclick="confirmDelete('{{$category->slug}}')">Delete</a>
          <form id="delete-category-{{$category->slug}}" action="{{ route('admin.category.destroy', $category->slug) }}" method="POST"
            style="display: none;">
            @method('DELETE') @csrf
          </form>
        </td>
        @endif
      </tr>
      @endforeach @else
      <tr>
        <td colspn="5">No Categories Found..</td>
      </tr>
      @endif
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-md-12">
    {{$categories->links()}}
  </div>
</div>
@endsection
 
@section('scripts')
<script type="text/javascript">
  function confirmDelete(id){ 
    let choice = confirm("Are You sure, You want to Delete this record ?") 
    if(choice){ 
      document.getElementById('delete-category-'+id).submit();
    } 
  }

</script>
@endsection