<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  @include('admin.partials.head')

<body>
  <div id="app">
    <!-- Side bar area -->
  @include('admin.partials.navbar')
    <!-- main content area -->
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-5">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h3 style="margin-top:0px;">
          <?php 
              echo str_replace(".","/",ucwords(Route::currentRouteName()));
          ?>
        </h3>
        @yield('content')
    </main>
    </div>


    <!-- footer area start -->
  @include('admin.partials.footer')

    <!-- selec2 function yield -->
    @yield('scripts')
</body>

</html>