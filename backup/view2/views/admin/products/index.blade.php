@extends('admin.app'); 
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <h2 class="h2">
      <?php 
        echo str_replace(".","/",ucwords(Route::currentRouteName()));
      ?>
    </h2>
  </div>
</main>
@endsection