<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  @include('admin.partials.head')

<body>
  <div id="app">


    <main class="py-4">
      <!-- Side bar area -->
  @include('admin.partials.navbar')
      <!-- main content area -->
      @yield('content')
    </main>
  </div>
  <!-- footer area start -->
  @include('admin.partials.footer')
</body>

</html>